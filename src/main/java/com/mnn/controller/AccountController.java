package com.mnn.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mnn.entities.CheckingAccount;
import com.mnn.entities.CheckingAccountTransaction;
import com.mnn.entities.SavingsAccount;
import com.mnn.entities.SavingsAccountTransaction;
import com.mnn.entities.User;
import com.mnn.service.AccountService;
import com.mnn.service.TransactionService;
import com.mnn.service.UserService;

@Controller
@RequestMapping("/account")
public class AccountController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private TransactionService transactionService;
	
	@RequestMapping("/checkingAccount")
	public String checkingAccount(Model model, Principal principal){
		String username = principal.getName();
		
		List<CheckingAccountTransaction> listTransaction = transactionService.findCheckingAccount(username);
		
		User user = userService.findByUsername(username);
		CheckingAccount account = user.getCheckingAccount();
		
		model.addAttribute("checkingAccount", account);
		model.addAttribute("listCheckingTransaction", listTransaction);
		
		return "checkingAccount";
	}

	@RequestMapping("/savingsAccount")
	public String savingsAccount(Model model, Principal principal){
		String username = principal.getName();
		
		List<SavingsAccountTransaction> listTransaction = transactionService.findSavingsAccount(username);
		
		User user = userService.findByUsername(username);
		SavingsAccount account = user.getSavingAccount();
		
		model.addAttribute("savingsAccount", account);
		model.addAttribute("listSavingTransaction", listTransaction);
		
		return "savingsAccount";
	}
	
	@RequestMapping(value = "/deposit", method = RequestMethod.GET)
	public String getDeposit(Model model){
		model.addAttribute("accountType", "");
		model.addAttribute("amount", "");
		
		return "deposit";
	}
	
	@RequestMapping(value = "/deposit", method = RequestMethod.POST)
	public String postDeposit(@ModelAttribute("amount") String amount, 
			@ModelAttribute("accountType") String accountType, 
			Principal principal){
		
		accountService.deposit(accountType, Double.parseDouble(amount), principal);
		
		return "redirect:/landingPage";
	}
	
	@RequestMapping(value = "/withdraw", method = RequestMethod.GET)
	public String getWithdraw(Model model){
		model.addAttribute("accountType", "");
		model.addAttribute("amount", "");
		
		return "withdraw";
	}
	
	@RequestMapping(value = "/withdraw", method = RequestMethod.POST)
	public String postWithdraw(@ModelAttribute("amount") String amount, 
			@ModelAttribute("accountType") String accountType, 
			Principal principal){
		
		accountService.withdraw(accountType, Double.parseDouble(amount), principal);
		
		return "redirect:/landingPage";
	}
	
	@ModelAttribute("userActive")
	public User getActiveUser(Principal principal){
		return getCurrentUser();
	}
	
	private User getCurrentUser(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		User user = userService.findByUsername(auth.getName());
		
		return user;
	}
}
