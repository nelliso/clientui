package com.mnn.controller;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mnn.entities.CheckingAccount;
import com.mnn.entities.SavingsAccount;
import com.mnn.entities.User;
import com.mnn.entities.security.UserRole;
import com.mnn.service.RoleService;
import com.mnn.service.UserService;

@Controller
public class HomeController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping("/")
	public String home(){
		return "redirect:/index";
	}
	
	@RequestMapping("/index")
	public String index(){
		return "index";
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Model model){
		User user = new User();
		
		model.addAttribute("user", user);
		
		return "signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signupPost(@ModelAttribute("user") User user, Model model){
		
		if(userService.checkIfUserExists(user.getUsername(), user.getEmail())){
			
			if(userService.checkIfEmailExists(user.getEmail())){
				model.addAttribute("emailExists", true);
			}
			
			if(userService.checkIfUsernameExists(user.getUsername())){
				model.addAttribute("usernameExists", true);
			}
			
			return "signup";
		}else{
			Set<UserRole> setUserRoles = new HashSet<>();
			setUserRoles.add(new UserRole(user, roleService.findByName("ROLE_USER")));
			
			userService.createUser(user, setUserRoles);
			
			return "redirect:/";
		}
	}
	
	@RequestMapping("/landingPage")
	public String landingPage(Principal principal, Model model){
		User user = userService.findByUsername(principal.getName());
		CheckingAccount checkingAccount = user.getCheckingAccount();
		SavingsAccount savingsAccount = user.getSavingAccount();
		
		model.addAttribute("checkingAccount", checkingAccount);
		model.addAttribute("savingsAccount", savingsAccount);
		
		return "landingPage";
	}
	
	@ModelAttribute("userActive")
	public User getActiveUser(Principal principal){
		return getCurrentUser();
	}
	
	private User getCurrentUser(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		User user = userService.findByUsername(auth.getName());
		
		return user;
	}
}
