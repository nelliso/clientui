package com.mnn.controller;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mnn.entities.Appointment;
import com.mnn.entities.User;
import com.mnn.service.AppointmentService;
import com.mnn.service.UserService;

@Controller
@RequestMapping("/appointment")
public class AppointmentController {
	
	@Autowired
	private AppointmentService appointmentService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String createAppointmentGet(Model model){
		Appointment appointment = new Appointment();
		model.addAttribute("appointment", appointment);
		model.addAttribute("dateString", "");
		
		return "appointment";
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createAppointmentPost(@ModelAttribute("appointment") Appointment appointment,
			@ModelAttribute("dateString") String date, Model model, Principal principal) throws ParseException{
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		Date d = sdf.parse(date);
		appointment.setDate(d);
		
		User user = userService.findByUsername(principal.getName());
		appointment.setUser(user);
		
		appointmentService.createAppointment(appointment);
		
		return "redirect:/landingPage";
		
	}
	
	@ModelAttribute("userActive")
	public User getActiveUser(Principal principal){
		return getCurrentUser();
	}
	
	private User getCurrentUser(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		User user = userService.findByUsername(auth.getName());
		
		return user;
	}
	
}
