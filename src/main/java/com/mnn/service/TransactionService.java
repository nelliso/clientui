package com.mnn.service;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.transaction.annotation.Transactional;

import com.mnn.entities.CheckingAccount;
import com.mnn.entities.CheckingAccountTransaction;
import com.mnn.entities.Recipient;
import com.mnn.entities.SavingsAccount;
import com.mnn.entities.SavingsAccountTransaction;

public interface TransactionService {
	List<CheckingAccountTransaction> findCheckingAccount(String username);
	List<SavingsAccountTransaction> findSavingsAccount(String username);
	void saveCheckingDepositTransaction(CheckingAccountTransaction accountTransaction);
	void saveSavingsDepositTransaction(SavingsAccountTransaction accountTransaction);
	void saveCheckingWithdrawTransaction(CheckingAccountTransaction accountTransaction);
	void saveSavingsWithdrawTransaction(SavingsAccountTransaction accountTransaction);
	void betweenAccountsTransfer(String transferFrom, String transferTo, String amount, 
			CheckingAccount checkingAccount, SavingsAccount savingsAccount) throws Exception;
	List<Recipient> findRecipientList(Principal principal);
	Recipient saveRecipient(Recipient recipient);
    Recipient findRecipientByName(String recipientName);
    void deleteRecipientByName(String recipientName);
    void toSomeoneTransfer(Recipient recipient, String accountType, String amount, 
    		CheckingAccount checkingAccount, SavingsAccount savingAccount);
}
