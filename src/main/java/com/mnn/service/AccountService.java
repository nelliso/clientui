package com.mnn.service;

import java.security.Principal;

import com.mnn.entities.CheckingAccount;
import com.mnn.entities.SavingsAccount;

public interface AccountService {
	
	CheckingAccount createCheckingAccount();
	SavingsAccount createSavingsAccount();
	void deposit(String accountType, double amount, Principal principal);
	void withdraw(String accountType, double amount, Principal principal);
}
