package com.mnn.service;

import com.mnn.entities.security.Role;

public interface RoleService{
	public Role findByName(String name);
}
