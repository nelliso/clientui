package com.mnn.service;

import com.mnn.entities.Appointment;

public interface AppointmentService {

	Appointment createAppointment(Appointment appointment);
	
	Appointment findAppointment(Long id);
	
	void confirmAppointment(Long id);

}
