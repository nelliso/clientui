package com.mnn.service;

import java.util.List;
import java.util.Set;

import com.mnn.entities.User;
import com.mnn.entities.security.UserRole;

public interface UserService {
	User findByUsername(String username);
	
	User findByEmail(String email);
	
	boolean checkIfUserExists(String username, String email);
	
	boolean checkIfUsernameExists(String username);
	
	boolean checkIfEmailExists(String email);
	
	void saveUser(User user);
	
	User createUser(User user, Set<UserRole> userRoles);

	void enableUser(String username);

	List<User> findUserList();

	void disableUser(String username);
}
