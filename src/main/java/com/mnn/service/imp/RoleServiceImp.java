package com.mnn.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mnn.dao.RoleDao;
import com.mnn.entities.security.Role;
import com.mnn.service.RoleService;

@Service
public class RoleServiceImp implements RoleService {

	@Autowired
	private RoleDao roleDao;
	
	@Override
	public Role findByName(String name) {
		return roleDao.findByName(name);
	}

}
