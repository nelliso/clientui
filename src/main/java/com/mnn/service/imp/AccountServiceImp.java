package com.mnn.service.imp;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mnn.dao.CheckingAccountDao;
import com.mnn.dao.SavingsAccountDao;
import com.mnn.entities.CheckingAccount;
import com.mnn.entities.CheckingAccountTransaction;
import com.mnn.entities.SavingsAccount;
import com.mnn.entities.SavingsAccountTransaction;
import com.mnn.entities.User;
import com.mnn.service.AccountService;
import com.mnn.service.TransactionService;
import com.mnn.service.UserService;

@Service
public class AccountServiceImp implements AccountService{
	
	private static int generateNewAccountNumber = 10000000;
	
	@Autowired
	private CheckingAccountDao checkingAccountDao;
	
	@Autowired
	private SavingsAccountDao savingsAccountDao;
	
	@Autowired
	UserService userService;
	
	@Autowired
	private TransactionService transactionService;

	@Override
	public CheckingAccount createCheckingAccount() {
		CheckingAccount checkingAccount = new CheckingAccount();
		checkingAccount.setAccountBalance(new BigDecimal(0.0));
		checkingAccount.setAccountNumber(accountGeneration());
		
		CheckingAccount accountSaved = checkingAccountDao.save(checkingAccount);
		
		return accountSaved;
	}

	@Override
	public SavingsAccount createSavingsAccount() {
		SavingsAccount savingsAccount = new SavingsAccount();
		savingsAccount.setAccountBalance(new BigDecimal(0.0));
		savingsAccount.setAccountNumber(accountGeneration());
		
		SavingsAccount accountSaved = savingsAccountDao.save(savingsAccount);
		
		return accountSaved;
	}

	
	private int accountGeneration(){
		return ++generateNewAccountNumber;
	}

	@Override
	public void deposit(String accountType, double amount, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		
		if(accountType.equalsIgnoreCase("Checking")){
			CheckingAccount checkingAccount = user.getCheckingAccount();
			checkingAccount.setAccountBalance(checkingAccount.getAccountBalance()
					.add(new BigDecimal(amount)));
			checkingAccountDao.save(checkingAccount);
			
			Date date = new Date();
			
			CheckingAccountTransaction accountTransaction = new CheckingAccountTransaction(
						date, "deposit to checking Account", "Account", "Completed", 
						amount, checkingAccount.getAccountBalance(), checkingAccount);
			
			transactionService.saveCheckingDepositTransaction(accountTransaction);
			
		}else if(accountType.equalsIgnoreCase("Savings")){
			SavingsAccount savingsAccount = user.getSavingAccount();
			savingsAccount.setAccountBalance(savingsAccount.getAccountBalance()
					.add(new BigDecimal(amount)));
			savingsAccountDao.save(savingsAccount);
			
			Date date = new Date();
			
			SavingsAccountTransaction accountTransaction = new SavingsAccountTransaction(
						date, "deposit to savings account", "Account", "Completed", 
						amount, savingsAccount.getAccountBalance(), savingsAccount);
			
			transactionService.saveSavingsDepositTransaction(accountTransaction);
		}
	}
	
	@Override
	public void withdraw(String accountType, double amount, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		
		if(accountType.equalsIgnoreCase("Checking")){
			CheckingAccount checkingAccount = user.getCheckingAccount();
			checkingAccount.setAccountBalance(checkingAccount.getAccountBalance()
					.subtract(new BigDecimal(amount)));
			checkingAccountDao.save(checkingAccount);
			
			Date date = new Date();
			
			CheckingAccountTransaction accountTransaction = new CheckingAccountTransaction(
						date, "withdraw from checking Account", "Account", "Completed", 
						amount, checkingAccount.getAccountBalance(), checkingAccount);
			
			transactionService.saveCheckingWithdrawTransaction(accountTransaction);
			
		}else if(accountType.equalsIgnoreCase("Savings")){
			SavingsAccount savingsAccount = user.getSavingAccount();
			savingsAccount.setAccountBalance(savingsAccount.getAccountBalance()
					.subtract(new BigDecimal(amount)));
			savingsAccountDao.save(savingsAccount);
			
			Date date = new Date();
			
			SavingsAccountTransaction accountTransaction = new SavingsAccountTransaction(
						date, "withdraw from savings account", "Account", "Completed", 
						amount, savingsAccount.getAccountBalance(), savingsAccount);
			
			transactionService.saveSavingsWithdrawTransaction(accountTransaction);
		}
		
	}
	
}
