package com.mnn.service.imp;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mnn.dao.RoleDao;
import com.mnn.dao.UserDao;
import com.mnn.entities.Recipient;
import com.mnn.entities.User;
import com.mnn.entities.security.UserRole;
import com.mnn.service.AccountService;
import com.mnn.service.UserService;

@Service
@Transactional
public class UserServiceImp implements UserService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImp.class);
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private RoleDao roleDao;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPWD;
	
	@Autowired
	private AccountService accountService;
	
	public User findByUsername(String username){
		return userDao.findByUsername(username);
	}
	
	public User findByEmail(String email){
		return userDao.findByEmail(email);
	}

	public boolean checkIfUserExists(String username, String email){
		if(checkIfUsernameExists(username) || checkIfEmailExists(username)){
			return true;
		}
		return false;
	}
	
	public boolean checkIfUsernameExists(String username){
		if(null != findByUsername(username)){
			return true;
		}
		return false;
	}
	
	public boolean checkIfEmailExists(String email){
		if(null != findByEmail(email)){
			return true;
		}
		return false;
	}

	@Override
	public void saveUser(User user) {
		
		userDao.save(user);
	}
	
	public User createUser(User user, Set<UserRole> userRoles){
		User userExist = userDao.findByUsername(user.getUsername());
		
		if(userExist != null){
			LOGGER.info("User with username {} already exist.", user.getUsername());
		}else{
			String encryptedPassword = bCryptPWD.encode(user.getPassword());
			user.setPassword(encryptedPassword);
			
			for(UserRole ur : userRoles){
				roleDao.save(ur.getRole());
			}
			
			user.getUserRoles().addAll(userRoles);
			
			user.setCheckingAccount(accountService.createCheckingAccount());
			user.setSavingAccount(accountService.createSavingsAccount());
			
			userExist = userDao.save(user);
		}
		return userExist;
	}

	@Override
	public void enableUser(String username) {
		User user = findByUsername(username);
		user.setEnabled(true);
		userDao.save(user);		
	}

	@Override
	public List<User> findUserList() {
		return userDao.findAll();
	}

	@Override
	public void disableUser(String username) {
		User user = findByUsername(username);
		user.setEnabled(false);
		userDao.save(user);				
	}

}
