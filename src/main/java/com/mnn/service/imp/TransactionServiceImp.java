package com.mnn.service.imp;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mnn.dao.CheckingAccountDao;
import com.mnn.dao.CheckingTransactionDao;
import com.mnn.dao.RecipientDao;
import com.mnn.dao.SavingsAccountDao;
import com.mnn.dao.SavingsTransactionDao;
import com.mnn.entities.CheckingAccount;
import com.mnn.entities.CheckingAccountTransaction;
import com.mnn.entities.Recipient;
import com.mnn.entities.SavingsAccount;
import com.mnn.entities.SavingsAccountTransaction;
import com.mnn.entities.User;
import com.mnn.service.TransactionService;
import com.mnn.service.UserService;

@Service
public class TransactionServiceImp implements TransactionService{
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CheckingTransactionDao checkingTransactionDao;
	
	@Autowired
	private SavingsTransactionDao savingsTransactionDao;
	
	@Autowired
	CheckingAccountDao checkingAccountDao;
	
	@Autowired
	SavingsAccountDao savingsAccountDao;
	
	@Autowired
	private  RecipientDao recipientDao;
	
	public List<CheckingAccountTransaction> findCheckingAccount(String username){
		User user = userService.findByUsername(username);
		List<CheckingAccountTransaction> transactionList = user.getCheckingAccount().getCheckingAccountTrans();
		
		return transactionList;
	}
	
	public List<SavingsAccountTransaction> findSavingsAccount(String username){
		User user = userService.findByUsername(username);
		List<SavingsAccountTransaction> transactionList = user.getSavingAccount().getListSavingsTransaction();
		
		return transactionList;
	}
	
	public void saveCheckingDepositTransaction(CheckingAccountTransaction accountTransaction){
		checkingTransactionDao.save(accountTransaction);
	}

	public void saveSavingsDepositTransaction(SavingsAccountTransaction accountTransaction){
		savingsTransactionDao.save(accountTransaction);
	}

	@Override
	public void saveCheckingWithdrawTransaction(CheckingAccountTransaction accountTransaction) {
		checkingTransactionDao.save(accountTransaction);
		
	}

	@Override
	public void saveSavingsWithdrawTransaction(SavingsAccountTransaction accountTransaction) {
		savingsTransactionDao.save(accountTransaction);
	}

	@Override
	public void betweenAccountsTransfer(String transferFrom, String transferTo, String amount,
			CheckingAccount checkingAccount, SavingsAccount savingsAccount) throws Exception {
		if (transferFrom.equalsIgnoreCase("Checking") && transferTo.equalsIgnoreCase("Savings")) {
			checkingAccount.setAccountBalance(checkingAccount.getAccountBalance().subtract(new BigDecimal(amount)));
            savingsAccount.setAccountBalance(savingsAccount.getAccountBalance().add(new BigDecimal(amount)));
            checkingAccountDao.save(checkingAccount);
            savingsAccountDao.save(savingsAccount);

            Date date = new Date();

            CheckingAccountTransaction primaryTransaction = new CheckingAccountTransaction(date, "Transfer from "+
            		transferFrom+" to "+transferTo, "Account", "Completed", Double.parseDouble(amount), checkingAccount.getAccountBalance(), 
            		checkingAccount);
            checkingTransactionDao.save(primaryTransaction);
        } else if (transferFrom.equalsIgnoreCase("Savings") && transferTo.equalsIgnoreCase("Checking")) {
        	checkingAccount.setAccountBalance(checkingAccount.getAccountBalance().add(new BigDecimal(amount)));
            savingsAccount.setAccountBalance(savingsAccount.getAccountBalance().subtract(new BigDecimal(amount)));
            checkingAccountDao.save(checkingAccount);
            savingsAccountDao.save(savingsAccount);

            Date date = new Date();

            SavingsAccountTransaction savingsTransaction = new SavingsAccountTransaction(date, "Transfer from "+transferFrom+" to "+transferTo, 
            		"Transfer", "Completed", Double.parseDouble(amount), savingsAccount.getAccountBalance(), savingsAccount);
            savingsTransactionDao.save(savingsTransaction);
        } else {
            throw new Exception("Invalid Transfer");
        }
	}
	
	public List<Recipient> findRecipientList(Principal principal) {
        String username = principal.getName();
        User user = userService.findByUsername(username);
        List<Recipient> recipientList = recipientDao.findRecipientList(user.getUserId());

        return recipientList;
    }

    public Recipient saveRecipient(Recipient recipient) {
        return recipientDao.save(recipient);
    }

    public Recipient findRecipientByName(String recipientName) {
        return recipientDao.findByName(recipientName);
    }

    @Transactional
    public void deleteRecipientByName(String recipientName) {
        recipientDao.deleteByName(recipientName);
    }

	@Override
	public void toSomeoneTransfer(Recipient recipient, String accountType, String amount,
			CheckingAccount checkingAccount, SavingsAccount savingAccount) {
		if (accountType.equalsIgnoreCase("Checking")) {
			checkingAccount.setAccountBalance(checkingAccount.getAccountBalance().subtract(new BigDecimal(amount)));
            checkingAccountDao.save(checkingAccount);

            Date date = new Date();

            CheckingAccountTransaction accountTransaction = new CheckingAccountTransaction(date, "Transfer to recipient "+
            		recipient.getName(), "Transfer", "Completed", Double.parseDouble(amount), 
            		checkingAccount.getAccountBalance(), checkingAccount);
            checkingTransactionDao.save(accountTransaction);
        } else if (accountType.equalsIgnoreCase("Savings")) {
        	savingAccount.setAccountBalance(savingAccount.getAccountBalance().subtract(new BigDecimal(amount)));
            savingsAccountDao.save(savingAccount);

            Date date = new Date();

            SavingsAccountTransaction savingsTransaction = new SavingsAccountTransaction(date, "Transfer to recipient "+
            		recipient.getName(), "Transfer", "Finished", Double.parseDouble(amount), savingAccount.getAccountBalance(), 
            		savingAccount);
            savingsTransactionDao.save(savingsTransaction);
        }
		
	}
	
}
