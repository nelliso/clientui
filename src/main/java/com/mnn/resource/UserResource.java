package com.mnn.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mnn.entities.CheckingAccountTransaction;
import com.mnn.entities.SavingsAccountTransaction;
import com.mnn.entities.User;
import com.mnn.service.TransactionService;
import com.mnn.service.UserService;

@RestController
@RequestMapping("/api")
//@PreAuthorize("hasRole('ADMIN')")
public class UserResource {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TransactionService transactionService;
	
	@RequestMapping(value = "/user/all", method = RequestMethod.GET)
	public List<User> userList(){
		return userService.findUserList();
	}
	
	@RequestMapping(value = "/user/checking/transaction", method = RequestMethod.GET)
	public List<CheckingAccountTransaction> getCheckingAccountTransactionList(
			@RequestParam("username") String username){
		return transactionService.findCheckingAccount(username);
	}
	
	@RequestMapping(value = "/user/savings/transaction", method = RequestMethod.GET)
	public List<SavingsAccountTransaction> getSavingsAccountTransactionList(
			@RequestParam("username") String username){
		return transactionService.findSavingsAccount(username);
	}
	
	@RequestMapping("/user/{username}/enable")
	public void enableUser(@PathVariable("username") String username){
		userService.enableUser(username);
	}
	
	@RequestMapping("/user/{username}/disable")
	public void disableUser(@PathVariable("username") String username){
		System.out.println("\n\n Username Disabled : " + username);
		userService.disableUser(username);
	}
}
