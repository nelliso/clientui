package com.mnn.dao;

import org.springframework.data.repository.CrudRepository;

import com.mnn.entities.CheckingAccountTransaction;

public interface CheckingTransactionDao extends CrudRepository<CheckingAccountTransaction, Long> {

}
