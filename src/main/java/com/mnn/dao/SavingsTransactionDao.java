package com.mnn.dao;

import org.springframework.data.repository.CrudRepository;

import com.mnn.entities.SavingsAccountTransaction;

public interface SavingsTransactionDao extends CrudRepository<SavingsAccountTransaction, Long> {

}
