package com.mnn.dao;

import org.springframework.data.repository.CrudRepository;

import com.mnn.entities.CheckingAccount;

public interface CheckingAccountDao extends CrudRepository<CheckingAccount, Long> {
	
	CheckingAccount findByAccountNumber(int number);
	
}
