package com.mnn.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.mnn.entities.Recipient;

public interface RecipientDao extends CrudRepository<Recipient, Long> {
	
	Recipient findByName(String name);
	void deleteByName(String name);
	@Query("select r from Recipient r left join r.user u where u.userId=?1")
	List<Recipient> findRecipientList(Long id);
}
