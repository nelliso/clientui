package com.mnn.dao;

import org.springframework.data.repository.CrudRepository;

import com.mnn.entities.security.Role;

public interface RoleDao extends CrudRepository<Role, Long> {
	Role findByName(String name);
}
