package com.mnn.dao;

import org.springframework.data.repository.CrudRepository;

import com.mnn.entities.SavingsAccount;

public interface SavingsAccountDao extends CrudRepository<SavingsAccount, Long> {
	SavingsAccount findByAccountNumber(int number);
}
