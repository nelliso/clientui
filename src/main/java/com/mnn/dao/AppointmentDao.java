package com.mnn.dao;

import org.springframework.data.repository.CrudRepository;

import com.mnn.entities.Appointment;

public interface AppointmentDao extends CrudRepository<Appointment, Long> {

}
