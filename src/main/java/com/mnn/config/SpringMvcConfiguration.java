package com.mnn.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

@Configuration
@EnableAutoConfiguration
@PropertySource("classpath:application.properties")
public class SpringMvcConfiguration extends WebMvcConfigurerAdapter{

		private static final String MESSAGESOURCE_BASENAME = "message.source.basename";
		private static final String MESSAGESOURCE_USE_CODE_AS_DEFAULT_MESSAGE = "message.source.use.code.as.default.message";
		
		@Autowired
		private Environment environment;
		
//		@Bean
//	    public TemplateResolver templateResolver(){
//	        ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver();
//	        templateResolver.setPrefix("/WEB-INF/templates/");
//	        templateResolver.setSuffix(".html");
//	        templateResolver.setTemplateMode("HTML5");
//	        templateResolver.setCharacterEncoding("UTF-8");
//	        templateResolver.setCacheable(false);
//	        return templateResolver;
//	    }
//
//	    @Bean
//	    public SpringTemplateEngine templateEngine(){
//	        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
//	        templateEngine.setTemplateResolver(templateResolver());
//	        return templateEngine;
//	    }
//
//		@Bean 
//		public ThymeleafViewResolver thymeleafViewResolver() {
//			ThymeleafViewResolver resolver = new ThymeleafViewResolver();
//			resolver.setTemplateEngine(templateEngine());
//			resolver.setContentType("text/html;charset=UTF-8");
//			resolver.setCharacterEncoding("UTF-8");
//			return resolver;
//		}
		
		@Bean
		public MessageSource messageSource() {
			ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
			messageSource.setBasename(environment.getRequiredProperty(MESSAGESOURCE_BASENAME));
			messageSource.setUseCodeAsDefaultMessage(
					Boolean.parseBoolean(environment.getRequiredProperty(MESSAGESOURCE_USE_CODE_AS_DEFAULT_MESSAGE)));
			return messageSource;
		}

		@Bean(name = "validator")
		public LocalValidatorFactoryBean validator() {
			LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
			bean.setValidationMessageSource(messageSource());
			return bean;
		}

		@Override
		public Validator getValidator() {
			return validator();
		}
}
